sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"com/demo/s276/ex04FileUploaderExample/libs/moment",
	"com/demo/s276/ex04FileUploaderExample/libs/xlsx",
	"com/demo/s276/ex04FileUploaderExample/libs/Blob",
	"com/demo/s276/ex04FileUploaderExample/libs/FileSaver"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("com.demo.s276.ex04FileUploaderExample.controller.View", {

		onUpload: function(e){
			var oThis = this;
			var fU = this.getView().byId("idfileUploader");
            var domRef = fU.getFocusDomRef();
			var f = domRef.files[0];
			var reader = new FileReader();
			reader.onload = function(e){
		    	var data = e.target.result;
		    	
		    	// 讀取 Excel 檔
		    	var workbook = XLSX.read(btoa(fixdata(data)), {type: "base64"});
		    	
		    	// 將名稱為 Template 的 sheet 資料轉為 JSON 格式
				var jsonData = XLSX.utils.sheet_to_json(workbook.Sheets["Template"]);
                //console.log(jsonData);
                
                var oModel = oThis.getOwnerComponent().getModel("ZUI5_DEMO_SRV");
                for(var key in jsonData){
                	var oEntry = {};
					oEntry.Zvblen = jsonData[key].ID;
					oEntry.Zerdat = jsonData[key].Date + "T00:00:00";
					oEntry.Zernam = jsonData[key].Name;
					oEntry.Zbstnk = jsonData[key].Reference;
					oModel.createEntry("/ZUI5_DEMO_TABLESet", {
						properties: oEntry,
						success: oThis._onCreateSuccess.bind(oThis),
						error: oThis._onCreateError.bind(oThis)
					});
    			}
    			oModel.submitChanges();
			};
			reader.readAsArrayBuffer(f); 
			
			function fixdata(data){ // 轉 BinaryString
		        var o = "", l = 0, w = 10240;
		        for( ; l < data.byteLength / w ; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
		        o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
		        return o;
		    }
		},
		
		_onCreateSuccess: function(o){
			console.log(o);
			var oModel = this.getOwnerComponent().getModel("ZUI5_DEMO_SRV");
			oModel.refresh();
		},
		
		_onCreateError: function(o){
			MessageToast.show("Upload Error");
			console.log(o);
			var oModel = this.getOwnerComponent().getModel("ZUI5_DEMO_SRV");
			oModel.refresh();
		},
		
		onDownload: function(){
			var ReportTitle = "Template";
			var aFinalXlsxData = [];
			aFinalXlsxData.push(["ID", "Date", "Name", "Reference"]);
			aFinalXlsxData.push([3, "2018-04-10", "Test", 12345]);
			aFinalXlsxData.push([4, "2018-04-10", "測試", 12345]);
			
			var Workbook = function Workbook(){
				if(!(this instanceof Workbook)) return new Workbook();
			    this.SheetNames = [];
			    this.Sheets = {};
			};
			var wb = Workbook();
			wb.SheetNames.push(ReportTitle);
			
			var sheet_from_array_of_arrays = function sheet_from_array_of_arrays(data, opts) {
				var ws = {};
			    var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
			    for(var R = 0 ; R !== data.length ; ++R){
			    	for(var C = 0 ; C !== data[R].length ; ++C){
			                    if(range.s.r > R) range.s.r = R;
			                    if(range.s.c > C) range.s.c = C;
			                    if(range.e.r < R) range.e.r = R;
			                    if(range.e.c < C) range.e.c = C;
			                    var cell = {v: data[R][C]};
			                    if(cell.v === null) continue;
			                    var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
			
			                    if(typeof cell.v === 'number') cell.t = 'n';
			                    else if(typeof cell.v === 'boolean') cell.t = 'b';
			                    else if(cell.v instanceof Date){
			                        cell.t = 'n'; cell.z = XLSX.SSF._table[14];
			                        cell.v = datenum(cell.v);
			                    }
			                    else cell.t = 's';
			                    ws[cell_ref] = cell;
			                }
			     }
			     if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
			     return ws;
			  };
			
			var ws = sheet_from_array_of_arrays(aFinalXlsxData);
			
			// Setting up Excel column width
			ws['!cols'] = [ 
				{wch:15},
			    {wch:15},
			    {wch:15},
			    {wch:15}
			];
			wb.Sheets[ReportTitle] = ws;
			
			// 寫入 Excel 檔
			var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
			
			// 儲存檔案
			var s2ab = function s2ab(s){
						var buf = new ArrayBuffer(s.length);
			            var view = new Uint8Array(buf);
			            for(var i=0 ; i !== s.length ; ++i) view[i] = s.charCodeAt(i) & 0xFF;
			            return buf;
			};
			saveAs(new Blob([s2ab(wbout)], {type:"application/octet-stream"}), ReportTitle + ".xlsx");
		},
		
		handleTypeMissmatch: function(oEvent){
			var aFileTypes = oEvent.getSource().getFileType();
			jQuery.each(aFileTypes, function(key, value){aFileTypes[key] = "*." +  value;});
			var sSupportedFileTypes = aFileTypes.join(", ");
			MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
									" is not supported. Choose one of the following types: " +
									sSupportedFileTypes);
		}

	});
});